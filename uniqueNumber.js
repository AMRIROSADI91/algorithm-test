function countUniqueValues(number) {
  let unique = [];

  for (let i = 0; i < number.length; i++) {
    if (number[i] !== number[i + 1]) {
      unique.push(number[i]);
    }
  }
  return unique.length;
}

console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
console.log(countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
console.log(countUniqueValues([]));
